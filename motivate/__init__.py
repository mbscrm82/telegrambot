"""

    This package has all the functionality for motivation.
    '/motivate' - displays user-friendly menu with available motivational commands;
    '/quote' - sends a random quote out of more than 4000 quotes;
    '/image' - sends random motivational image out of more than 200 images;
    '/sound' - sends random motivational speech;
    '/book' - sends only a one book for now.\

"""